<?php 

require '../connect.php';

$int = (int)$_POST['id'];

$job_id = $int;


if ($job_id != null || $job_id != "") {

    $sql = "SELECT * FROM ticket WHERE id = $job_id";
    $result = $dbcon->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $room = $row['room'];
            $item = $row['item'];
            $detail = $row['detail'];
            $serial_num = $row['serial_num'];
            $submitted_name = $row['submitted_name'];
            $created_at = $row['created_at'];
             // LINE NOTIFICATION START -----------------------------------------------
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
    date_default_timezone_set("Asia/Bangkok");

	$sToken = "JczwUZmHhG9mAs2M6BE4Q8TkJAIriLRYR22WSTOU3rE";
    $sMessage = "แจ้งเตือนการแจ้งซ่อม
ห้อง: $room
สิ่งของ: $item
รายละเอียด: $detail
รหัสสิ่งของ: $serial_num
แจ้งซ่อมโดย: $submitted_name
เวลา: $created_at";

	$chOne = curl_init(); 
	curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
	curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
	curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
	curl_setopt( $chOne, CURLOPT_POST, 1); 
	curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=".$sMessage); 
	$headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$sToken.'', );
	curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
	curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
	$result = curl_exec( $chOne ); 

	//Result error 
	if(curl_error($chOne)) 
	{ 
		echo 'error:' . curl_error($chOne); 
	}
	curl_close( $chOne );   
    // LINE NOTIFICATION STOP -----------------------------------------------
    
    echo "<script>
    alert('แจ้งเตือนพนักงานอีกครั้ง สำเร็จ');
    window.history.back();
</script>";
        }
        
    }
    else {
        echo "<script>
            alert('เกิดข้อผิดพลาด');
            window.history.back();
        </script>";
        }
}  else {
    echo "<script>
        alert('เกิดข้อผิดพลาด');
        window.history.back();
    </script>";
    }

?>